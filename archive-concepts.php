<?php get_header(); ?>

    <?php get_template_part('templates/concepts/page-header'); ?>
    
    <?php get_template_part('templates/concepts/nav'); ?>

    <?php get_template_part('templates/concepts/concepts'); ?>

<?php get_footer(); ?>