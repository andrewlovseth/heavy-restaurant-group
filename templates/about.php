<?php 

/*

    Template Name: About

*/

get_header(); ?>

    <?php get_template_part('templates/about/page-header'); ?>

    <?php get_template_part('templates/about/people'); ?>

    <?php get_template_part('templates/about/directory'); ?>

    <?php // get_template_part('templates/about/get-in-touch'); ?>

<?php get_footer(); ?>
