<?php

    $page_header = get_field('page_header');
    $headline = $page_header['headline'];
    $copy = $page_header['copy'];

?>

<section class="page-header grid">
        <div class="concepts-mobile-snap-point snap-point"></div>

    <?php if($headline): ?>
        <div class="headline">
            <h1 class="upper-title"><?php echo $headline; ?></h1>
        </div>
    <?php endif; ?>

    <?php if($copy): ?>
        <div class="copy copy-1">
            <?php echo $copy; ?>
        </div>
    <?php endif; ?>
</section>