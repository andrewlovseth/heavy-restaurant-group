<?php 

    $hero = get_field('hero');
    $headline = $hero['headline'];
    $icon = $hero['icon'];

?>

<section class="hero">

    <div class="headline js-fade-in">
        <h1 class="section-title"><?php echo $headline; ?></h1>
    </div>

    <div class="icon js-fade-in delay-1000">
        <?php echo wp_get_attachment_image($icon['ID'], 'full'); ?>
    </div>

    <div class="down-arrow js-fade-in delay-1500">
        <?php get_template_part('svg/icon-down-arrow'); ?>
    </div>


</section>