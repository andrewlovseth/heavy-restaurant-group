<section class="concepts grid">

    <div class="concepts-0-snap-point snap-point"></div>
    <div class="concepts-50-snap-point snap-point"></div>
    <div class="concepts-100-snap-point snap-point"></div>

    <div class="section-header concepts-section-header concepts-section-header-mobile">
        <h2 class="section-title concepts-section-title concepts-section-title-mobile" data-init-text="<?php echo get_field('concepts_headline'); ?>" data-modified-text="<?php echo get_field('concepts_headline_modified'); ?>"><?php echo get_field('concepts_headline'); ?></h2>
    </div>

    <div class="section-header concepts-section-header concepts-section-header-desktop">
        <h2 class="section-title concepts-section-title concepts-section-title-desktop" data-init-text="<?php echo get_field('concepts_headline'); ?>" data-modified-text="<?php echo get_field('concepts_headline_modified'); ?>"><?php echo get_field('concepts_headline'); ?></h2>
    </div>


    <?php if(have_rows('concepts')): $count = 1; while(have_rows('concepts')) : the_row(); ?>

        <?php if( get_row_layout() == 'concept' ): ?>

            <?php 
                $slug = get_sub_field('slug');
                $photo = get_sub_field('photo');

                $stickers = get_sub_field('stickers');
                $pins = get_sub_field('pins');

                $classList = "concept concept-" . $slug;

                if ($count % 2 != 0) {
                    $classList .= " odd";
                }
            ?>

            <section class="<?php echo $classList; ?>">
                <div class="photo">
                    <?php echo wp_get_attachment_image($photo['ID'], 'full', false, array( 'class' => 'js-fade-in') ); ?>

                    <?php if($stickers): ?>

                        <?php $sticker_count = 1; foreach($stickers as $sticker): ?>
                            <?php
                                $image = $sticker['image'];
                                $url = $sticker['image']['title'];
                                $sticker_slug = sanitize_title_with_dashes($sticker['image']['alt']);
                            ?>

                            <div class="js-fade-in delay-1000 duration-600 sticker sticker-<?php echo $sticker_count; ?> <?php echo $sticker_slug; ?>">
                                <?php if($url): ?>
                                    <a href="<?php echo $url; ?>" rel="external">
                                        <?php echo wp_get_attachment_image($image['ID'], 'full'); ?>
                                    </a>                            
                                <?php else: ?>
                                    <?php echo wp_get_attachment_image($image['ID'], 'full'); ?>
                                <?php endif; ?>
                            </div>

                        <?php $sticker_count++; endforeach; ?>

                    <?php endif; ?>
                    
                    <?php if($pins): ?>

                        <?php $pin_count = 1; foreach($pins as $pin): ?>
                            <?php
                                $image = $pin['image'];
                                $url = $pin['image']['title'];
                                $pin_slug = sanitize_title_with_dashes($pin['image']['alt']);
                            ?>

                            <div class="js-fade-in delay-1500 duration-800 pin pin-<?php echo $pin_count; ?> <?php echo $pin_slug; ?>">
                                <?php if($url): ?>
                                    <a href="<?php echo $url; ?>" rel="external">
                                        <?php echo wp_get_attachment_image($image['ID'], 'full'); ?>
                                    </a>                            
                                <?php else: ?>
                                    <?php echo wp_get_attachment_image($image['ID'], 'full'); ?>
                                <?php endif; ?>
                            </div>

                        <?php $pin_count++; endforeach; ?>

                    <?php endif; ?>                
                </div>

            </section>

        <?php endif; ?>

    <?php $count++; endwhile; endif; ?>

</section>