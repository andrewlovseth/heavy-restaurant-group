<section class="menus grid">
    <div class="section-header">
        <h3 class="upper-title"><?php echo get_field('menus_headline'); ?></h3>
    </div>

    <div class="menu-grid">
        <?php if(have_rows('menus')): while(have_rows('menus')): the_row(); ?>

            <?php
                $menu = get_sub_field('file');
                $menu_title = $menu['title'];
                $menu_url = $menu['url'];
            ?>
        
            <div class="menu cta">
                <a href="<?php echo $menu_url; ?>" class="btn blue" target="window"><?php echo $menu_title; ?></a>                
            </div>

        <?php endwhile; endif; ?>
    </div>
</section>

