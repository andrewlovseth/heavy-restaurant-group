<?php

    $request_form = get_field('request_form');
    $headline = $request_form['headline'];   
    $form = $request_form['form'];

?>

<section class="request-form grid">
    <div class="form-header">
        <h3 class="upper-title"><?php echo $headline; ?></h3>
    </div>

    <?php echo do_shortcode($form); ?>
</section>