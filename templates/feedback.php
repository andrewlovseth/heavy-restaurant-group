<?php 

/*

    Template Name: Feedback

*/

get_header(); ?>

    <?php get_template_part('template-parts/global/page-header'); ?>

    <?php get_template_part('templates/feedback/form'); ?>

<?php get_footer(); ?>