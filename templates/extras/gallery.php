<section class="gallery grid">
    <div class="gallery-container">

        <?php if(have_rows('gallery')): $delay = 500; while(have_rows('gallery')): the_row(); ?>
    
            <div class="item js-fade-in" style="transition-delay: <?php echo $delay; ?>ms;">
                <a href="<?php echo get_sub_field('link'); ?>" target="window">
                    <?php $image = get_sub_field('image');echo wp_get_attachment_image($image['ID'], 'full'); ?>
                </a>
            </div>

        <?php $delay = $delay + 250; endwhile; endif; ?>

    </div>
</section>