<?php 

/*

    Template Name: Group Dining

*/

get_header(); ?>

    <?php get_template_part('template-parts/global/page-header'); ?>
    
    <?php get_template_part('template-parts/global/gallery'); ?>

    <?php get_template_part('templates/group-dining/group-dining-menus'); ?>

    <?php get_template_part('templates/group-dining/request-form'); ?>

<?php get_footer(); ?>