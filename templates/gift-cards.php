<?php 

/*

    Template Name: Gift Cards

*/

get_header(); ?>

    <?php get_template_part('template-parts/global/page-header'); ?>

    <?php get_template_part('templates/gift-cards/products'); ?>

<?php get_footer(); ?>