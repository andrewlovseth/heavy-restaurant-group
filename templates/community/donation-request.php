<?php

    $donation_request = get_field('donation_request');
    $form_headline = $donation_request['form_headline'];   
    $form = $donation_request['form'];

?>

<section class="donation-request grid js-fade-in delay-1500" id="giving-back">
    <div class="form-header">
        <h3 class="upper-title"><?php echo $form_headline; ?></h3>
    </div>

    <?php echo do_shortcode($form); ?>
</section>