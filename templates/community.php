<?php 

/*

    Template Name: Community

*/

get_header(); ?>

    <?php get_template_part('template-parts/global/page-header'); ?>

    <?php get_template_part('templates/community/donation-request'); ?>

<?php get_footer(); ?>