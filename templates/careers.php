<?php 

/*

    Template Name: Careers

*/

get_header(); ?>

    <?php get_template_part('template-parts/global/page-header'); ?>

    <?php get_template_part('template-parts/global/gallery'); ?>

    <?php get_template_part('templates/careers/featured-person'); ?>

    <?php get_template_part('templates/careers/benefits'); ?>

    <?php get_template_part('templates/careers/opportunities'); ?>

<?php get_footer(); ?>