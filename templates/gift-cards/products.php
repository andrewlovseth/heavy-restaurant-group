<?php if(have_rows('products')): ?>
    
    <section class="products grid">
        <div class="product-wrapper">

            <?php while(have_rows('products')) : the_row(); ?>

                <?php if( get_row_layout() == 'product' ): ?>
                    
                    <?php
                        $photo = get_sub_field('photo');
                        $name = get_sub_field('name');
                        $link = get_sub_field('link');
                    ?>

                    <div class="product">
                        <a href="<?php echo $link; ?>" target="window">
                            <div class="photo">
                                <?php echo wp_get_attachment_image($photo['ID'], 'medium'); ?>
                            </div>

                            <div class="info">
                                <span class="name"><?php echo $name; ?></span>
                            </div>
                        </a>
                    </div>

                <?php endif; ?>

            <?php endwhile; ?>

        </div>
    </section>

<?php endif; ?>