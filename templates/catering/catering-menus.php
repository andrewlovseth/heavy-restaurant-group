<section class="menus grid">
    <div class="section-header">
        <h3 class="upper-title"><?php echo get_field('menus_headline'); ?></h3>
    </div>

    <div class="menu-grid">
        <?php if(have_rows('menus')): while(have_rows('menus')): the_row(); ?>

            <?php 
                $link = get_sub_field('link');
                if( $link ): 
                $link_url = $link['url'];
                $link_title = $link['title'];
                $link_target = $link['target'] ? $link['target'] : '_self';
            ?>

                <div class="menu cta">
                    <a class="btn blue" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
                </div>

            <?php endif; ?>

        <?php endwhile; endif; ?>
    </div>
</section>

