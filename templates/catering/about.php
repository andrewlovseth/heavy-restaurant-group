<?php

    $about = get_field('about');
    $headline = $about['headline'];
    $sub_headline = $about['sub_headline'];
    $copy = $about['copy'];
    $list_headline = $about['list_headline'];
    $list = $about['list'];
    $link = $about['cta'];

?>

<section class="about grid">
    <div class="section-header">
        <h2 class="section-title"><?php echo $headline; ?></h2>
    </div>

    <div class="main">
        <div class="headline">
            <h3><?php echo $sub_headline; ?></h3>
        </div>

        <div class="copy copy-2">
            <?php echo $copy; ?>
        </div>
    </div>

    <div class="sidebar">
        <div class="sidebar-headline">
            <h4><?php echo $list_headline; ?></h4>
        </div>

        <div class="copy copy-3">
            <?php echo $list; ?>
        </div>

        <?php 
            if( $link ): 
            $link_url = $link['url'];
            $link_title = $link['title'];
            $link_target = $link['target'] ? $link['target'] : '_self';
        ?>

            <div class="cta">
                <a class="btn blue" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
            </div>

        <?php endif; ?>
    </div>
</section>