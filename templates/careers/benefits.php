<?php

    $benefits = get_field('benefits');
    $headline = $benefits['headline'];
    $list = $benefits['list'];

?>

<section class="benefits grid">
    <div class="main">
        <div class="headline">
            <h3><?php echo $headline; ?></h3>
        </div>

        <div class="copy copy-3">
            <?php echo $list; ?>
        </div>
    </div>
</section>