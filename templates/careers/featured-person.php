<?php

    $featured_person = get_field('featured_person');
    $photo = $featured_person['photo'];
    $headline = $featured_person['headline'];
    $copy = $featured_person['copy'];

?>

<section class="featured-person grid">
    <div class="photo js-fade-in">
        <?php echo wp_get_attachment_image($photo['ID'], 'large'); ?>
    </div>

    <div class="info">
        <div class="headline js-fade-in delay-1000">
            <h3><?php echo $headline; ?></h3>
        </div>

        <div class="copy copy-2 js-fade-in delay-1000">
            <?php echo $copy; ?>
        </div>
    </div>
</section>