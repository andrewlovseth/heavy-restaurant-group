<?php

    $opportunities = get_field('opportunities');
    $headline = $opportunities['headline'];   
    $form = $opportunities['form'];

?>

<section class="opportunities grid">
    <div class="form-header">
        <h3 class="upper-title"><?php echo $headline; ?></h3>
    </div>

    <?php echo do_shortcode($form); ?>
</section>