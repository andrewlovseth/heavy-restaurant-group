<?php

    $cloned_group = get_field('page_header');
    $page_header = $cloned_group['page_header'];
    $headline = $page_header['headline'];
    $copy = $page_header['copy'];

?>

<section class="page-header grid">
    <?php if($headline): ?>
        <div class="headline js-fade-in">
            <h1 class="upper-title"><?php echo $headline; ?></h1>
        </div>
    <?php endif; ?>

    <?php if($copy): ?>
        <div class="copy copy-1 js-fade-in delay-1000">
            <?php echo $copy; ?>
        </div>
    <?php endif; ?>

    <?php 
        $link = get_field('get_in_touch_anchor');
        if( $link ): 
        $link_url = $link['url'];
        $link_title = $link['title'];
        $link_target = $link['target'] ? $link['target'] : '_self';
    ?>

        <div class="anchor js-fade-in delay-1500">
            <a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
        </div>

    <?php endif; ?>
</section>