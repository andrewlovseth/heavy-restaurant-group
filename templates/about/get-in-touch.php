<?php

    $get_in_touch = get_field('get_in_touch');
    $headline = $get_in_touch['headline'];
    $copy = $get_in_touch['copy'];
    $form_headline = $get_in_touch['form_headline'];   
    $form = $get_in_touch['form'];

    $contact_info = get_field('contact_info');
    $location = $contact_info['location'];
    $address = $contact_info['address'];
    $phone = $contact_info['phone'];
    $email = $contact_info['email'];


?>

<section id="get-in-touch" class="get-in-touch grid js-fade-in">
    <div class="section-header">
        <div class="headline">
            <h2 class="section-title"><?php echo $headline; ?></h2>
        </div>

        <div class="copy copy-1 small">
            <?php echo $copy; ?>
        </div>
    </div>

    <div class="contact-info">
        <?php if($location): ?>
            <div class="location">
                <h5><?php echo $location; ?></h5>
            </div>
        <?php endif; ?>

        <p class="copy copy-2">
            <?php if($address): ?>
                <span class="address"><?php echo $address; ?></span>
            <?php endif; ?>

            <?php if($phone): ?>
                <span class="phone"><?php echo $phone; ?></span>
            <?php endif; ?>

            <?php if( $email ): ?>
                <a class="email" href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a>
            <?php endif; ?>
        </p>
    </div>
    
    <div class="form-header">
        <h3 class="upper-title"><?php echo $form_headline; ?></h3>
    </div>

    <?php echo do_shortcode($form); ?>
</section>