<?php

    $people = get_field('people');
    if($people):

?>

    <section class="people people-mobile js-people-slider">
        <div class="swiper-wrapper">
            <?php foreach($people as $person): ?>

                <?php
                    $photo = get_field('photo', $person->ID);
                    $name = get_the_title($person->ID);
                    $title = get_field('title', $person->ID);
                ?>

                <div class="person swiper-slide">
                    <div class="photo">
                        <?php echo wp_get_attachment_image($photo['ID'], 'medium'); ?>
                    </div>

                    <div class="info">
                        <div class="name">
                            <h4><?php echo $name; ?></h4>
                        </div>

                        <div class="title">
                            <h5><?php echo $title; ?></h5>
                        </div>
                    </div>
                </div>


            <?php endforeach; ?>
        </div>
    </section>

    <section class="people people-desktop grid">
        <div class="container">
            <?php foreach($people as $person): ?>

                <?php
                    $photo = get_field('photo', $person->ID);
                    $name = get_the_title($person->ID);
                    $title = get_field('title', $person->ID);
                ?>

                <div class="person">
                    <div class="photo">
                        <?php echo wp_get_attachment_image($photo['ID'], 'medium'); ?>
                    </div>

                    <div class="info">
                        <div class="name">
                            <h4><?php echo $name; ?></h4>
                        </div>

                        <div class="title">
                            <h5><?php echo $title; ?></h5>
                        </div>
                    </div>
                </div>


            <?php endforeach; ?>
        </div>
    </section>

<?php endif; ?>