<?php

    $directory_info = get_field('directory_info');
    $headline = $directory_info['headline'];
    $copy = $directory_info['copy'];

?>

<section class="directory grid">
    <div class="section-header">
        <div class="headline">
            <h2 class="section-title"><?php echo $headline; ?></h2>
        </div>

        <div class="copy copy-2">
            <?php echo $copy; ?>
        </div>
    </div>

    <?php if(have_rows('directory')): ?>

        <div class="directory__list">

            <?php while(have_rows('directory')) : the_row(); ?>

                <?php if( get_row_layout() == 'contact' ): ?>
                    <?php
                        $header = get_sub_field('header');
                        $name = get_sub_field('name');
                        $email = get_sub_field('email');
                        $phone = get_sub_field('phone');
                    ?>

                    <div class="directory__item">
                        <div class="directory__item-header">
                            <h4><?php echo $header; ?></h4>
                        </div>

                        <div class="directory__item-body">
                            <p>
                                <span class="directory__item-name"><?php echo $name; ?></span>
                                <a href="mailto:<?php echo $email; ?>" class="directory__item-email"><?php echo $email; ?></a>
                                <span class="directory__item-phone"><?php echo $phone; ?></span>                                
                            </p>
                        </div>
                    </div>

                <?php endif; ?>

            <?php endwhile; ?>

        </div>

    <?php endif; ?>

</section>