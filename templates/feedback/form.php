<?php

    $feedback_form = get_field('feedback_form');
    $form = $feedback_form['form'];

?>

<section class="feedback-form grid js-fade-in delay-1500">
    <?php echo do_shortcode($form); ?>
</section>