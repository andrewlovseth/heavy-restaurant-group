<?php

    $restaurants = get_field('restaurants', 'options');
    if($restaurants):

?>

    <?php $count = 1; foreach($restaurants as $restaurant): ?>

        <?php
            $display_name = get_field('display_name', $restaurant['restaurant']->ID);
            $slug = $restaurant['slug'];
            $tagline = get_field('tagline', $restaurant['restaurant']->ID);
            $description = get_field('description', $restaurant['restaurant']->ID);
            $locations = get_field('locations', $restaurant['restaurant']->ID);

            if($locations) {
                $locations_count = count(get_field('locations', $restaurant['restaurant']->ID));
            }
            
            $gallery = get_field('gallery', $restaurant['restaurant']->ID);
        ?>

        <?php if($slug): ?>
            <section id="<?php echo $slug; ?>" class="concept grid">
        <?php else: ?>
            <section class="concept grid">
        <?php endif; ?>

            <?php
                $args = [
                    'display_name' => $display_name,
                    'tagline' => $tagline,
                    'description' => $description,
                ];
                get_template_part('templates/concepts/concept/info', null, $args);
            ?>

            <?php
                $args = [
                    'locations' => $locations,
                    'locations_count' => $locations_count,
                ];
                get_template_part('templates/concepts/concept/locations', null, $args);
            ?>

            <?php
                $args = [
                    'gallery' => $gallery,
                    'count' => $count,
                ];
                get_template_part('templates/concepts/concept/gallery', null, $args);
            ?>

        </section>   

    <?php $count++; endforeach; ?>

<?php endif; ?>