<?php

$args = wp_parse_args($args);

if(!empty($args)) {
    $locations = $args['locations']; 
    $locations_count = $args['locations_count']; 
}

if($locations): ?>

    <div class="contact-info tabs">

        <?php if($locations_count > 1): ?>

            <div class="tab-list">
                <?php $count = 1; foreach($locations as $location): ?>

                    <?php
                        $neighborhood = $location['neighborhood'];
                        $slug = sanitize_title_with_dashes($neighborhood);
                        $abbr = $location['abbr'];
                    ?>

                    <a href="#" <?php if($count === 1): ?>aria-selected="true" <?php endif; ?>role="tab" id="<?php echo $slug; ?>">
                        <span><?php echo $abbr; ?></span>
                    </a>

                <?php $count++; endforeach; ?>
            </div>

        <?php endif; ?>


        <?php $count = 1; foreach($locations as $location): ?>
            <?php
                $neighborhood = $location['neighborhood'];
                $slug = sanitize_title_with_dashes($neighborhood);

                $pin = $location['pin'];
                $address = $location['address'];
                $phone = $location['phone'];
                $website = $location['website'];
                $reservations = $location['reservations'];
                $social = $location['social'];
            ?>
    
            <div class="tab-panel" role="tabpanel" aria-labelledby="<?php echo $slug; ?>"<?php if($count > 1): ?> hidden<?php endif; ?>>

                <div class="info">
                    <?php if($neighborhood): ?>
                        <div class="neighborhood">
                            <h5><?php echo $neighborhood; ?></h5>
                        </div>
                    <?php endif; ?>

                    <p class="copy copy-2">
                        <?php if($address): ?>
                            <span class="address"><?php echo $address; ?></span>
                        <?php endif; ?>

                        <?php if($phone): ?>
                            <span class="phone"><?php echo $phone; ?></span>
                        <?php endif; ?>

                        <?php 
                            if( $website ): 
                            $website_url = $website['url'];
                            $website_title = $website['title'];
                            $website_target = $website['target'] ? $website['target'] : '_self';
                        ?>
                            <a class="website" href="<?php echo esc_url($website_url); ?>" target="<?php echo esc_attr($website_target); ?>"><?php echo esc_html($website_title); ?></a>
                        <?php endif; ?>
                    </p>
                    
                    <?php if($social): ?>
                        <div class="social">
                            <?php foreach($social as $social_link): ?>

                                <?php 
                                    $link = $social_link['link'];
                                    if( $link ): 
                                    $link_url = $link['url'];
                                    $link_title = $link['title'];
                                    $link_slug = sanitize_title_with_dashes($link_title); 
                                    $link_target = $link['target'] ? $link['target'] : '_self';
                                ?>

                                    <div class="social-link">
                                        <a class="<?php echo $link_slug; ?>" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>">
                                            <?php get_template_part('svg/' . $link_slug); ?>
                                        </a>
                                    </div>

                                <?php endif; ?>

                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>
                </div>

            </div>

        <?php $count++; endforeach; ?>
    </div>
    
<?php endif; ?>