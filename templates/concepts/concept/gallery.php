<?php 
                          
$args = wp_parse_args($args);

if(!empty($args)) {
    $gallery = $args['gallery']; 
    $count = $args['count']; 
}

if( $gallery ): ?>

    <div class="gallery">
        <div class="gallery-container swiper<?php if ($count % 2 == 0): ?>-reverse<?php endif; ?>">
            <div class="swiper-wrapper">
        
                <?php foreach( $gallery as $photo ): ?>

                    <?php    
                        $photoClass = 'photo swiper-slide';
                        $title = $photo['title'];

                        if($title === 'medium') {
                            $photoClass .= ' medium';
                        }
                        if($title === 'large') {
                            $photoClass .= ' large';
                        }                        
                    ?>

                    <div class="<?php echo $photoClass; ?>">
                        <a data-fslightbox href="<?php echo wp_get_attachment_image_url($photo['id'], 'full'); ?>">
                            <img src="<?php echo $photo['sizes']['medium']; ?>" alt="<?php echo $photo['alt']; ?>" />
                        </a>
                    </div>

                <?php endforeach; ?>
                
            </div>
        </div>
    </div>
    
<?php endif; ?>