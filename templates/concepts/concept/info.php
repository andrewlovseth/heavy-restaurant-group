<?php

$args = wp_parse_args($args);

if(!empty($args)) {
    $display_name = $args['display_name']; 
    $tagline = $args['tagline']; 
    $description = $args['description']; 
}

?>

<div class="section-header">
    <?php if($display_name): ?>
        <div class="name">
            <h3 class="section-title"><?php echo $display_name; ?></h3>
        </div>
    <?php endif; ?>

    <?php if($tagline): ?>
        <div class="tagline">
            <h4><?php echo $tagline; ?></h4>
        </div>
    <?php endif; ?>
</div>

<?php if($description): ?>
    <div class="description copy copy-2 large">
        <?php echo $description; ?>
    </div>
<?php endif; ?>