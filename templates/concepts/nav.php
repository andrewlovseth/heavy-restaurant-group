<?php

    $restaurants = get_field('restaurants', 'options');

?>

<nav class="concept-nav grid js-fade-in delay-1500">
    <div class="links">
        <ul>
            <?php foreach($restaurants as $restaurant): ?>
                <?php
                    $title = get_field('display_name', $restaurant['restaurant']->ID);
                    $slug = $restaurant['slug'];
                    if($slug):
                ?>

                    <li>
                        <a href="#<?php echo $slug; ?>"><?php echo $title; ?></a>
                    </li>
                
                <?php endif; ?> 

            <?php endforeach; ?>
        </ul>
    </div>
</nav>