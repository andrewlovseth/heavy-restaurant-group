<?php 

/*

    Template Name: Home

*/

get_header(); ?>

    <?php get_template_part('templates/home/hero'); ?>

    <?php get_template_part('templates/home/page-header'); ?>

    <?php get_template_part('templates/home/concepts'); ?>

<?php get_footer(); ?>