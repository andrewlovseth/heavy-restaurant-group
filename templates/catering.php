<?php 

/*

    Template Name: Catering

*/

get_header(); ?>

    <?php get_template_part('template-parts/global/page-header'); ?>

    <?php get_template_part('template-parts/global/gallery'); ?>

    <?php get_template_part('templates/catering/about'); ?>

    <?php get_template_part('templates/catering/catering-menus'); ?>

    <?php get_template_part('templates/catering/request-form'); ?>

<?php get_footer(); ?>