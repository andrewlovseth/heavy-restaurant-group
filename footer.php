	</main> <!-- .site-content -->

	<footer class="site-footer grid">

		<?php get_template_part('template-parts/footer/logo'); ?>

		<?php get_template_part('template-parts/footer/subscribe'); ?>

		<?php get_template_part('template-parts/footer/restaurant-group'); ?>

		<?php get_template_part('template-parts/footer/contact-info'); ?>
		
		<?php get_template_part('template-parts/footer/photo-credit'); ?>

	</footer>

<?php wp_footer(); ?>

</div> <!-- .site -->

</body>
</html>