<?php

    $cloned_group = get_field('page_header');
    $page_header = $cloned_group['page_header'];
    $headline = $page_header['headline'];
    $copy = $page_header['copy'];

?>

<section class="page-header grid">
    <?php if($headline): ?>
        <div class="headline js-fade-in">
            <h1 class="upper-title"><?php echo $headline; ?></h1>
        </div>
    <?php endif; ?>

    <?php if($copy): ?>
        <div class="copy copy-1 js-fade-in delay-1000">
            <?php echo $copy; ?>
        </div>
    <?php endif; ?>
</section>