<?php
    $gallery = get_field('gallery');

    if( $gallery ):
?>

    <div class="gallery">
        <div class="gallery-container swiper">
            <div class="swiper-wrapper">
        
                <?php foreach( $gallery as $photo ): ?>

                    <?php    
                        $photoClass = 'photo swiper-slide';
                        $title = $photo['title'];

                        if($title === 'medium') {
                            $photoClass .= ' medium';
                        }
                        if($title === 'large') {
                            $photoClass .= ' large';
                        }                        
                    ?>

                    <div class="<?php echo $photoClass; ?>">
                        <a data-fslightbox href="<?php echo wp_get_attachment_image_url($photo['id'], 'full'); ?>">
                            <img src="<?php echo $photo['sizes']['medium']; ?>" alt="<?php echo $photo['alt']; ?>" />
                        </a>
                    </div>

                <?php endforeach; ?>
                
            </div>

        </div>
    </div>

<?php endif; ?>