<nav class="site-nav">
    <div class="logo-mark">
        <a href="<?php echo site_url('/'); ?>">
            <?php $logo = get_field('logo_mark', 'options'); echo wp_get_attachment_image($logo['ID'], 'full'); ?>
        </a>
    </div>

    <ul role="navigation">
        <?php if(have_rows('nav', 'options')): while(have_rows('nav', 'options')): the_row(); ?>
 
            <?php 
                $link = get_sub_field('link');
                if( $link ): 
                $link_url = $link['url'];
                $link_title = $link['title'];
                $link_slug = sanitize_title_with_dashes($link_title);
                $link_target = $link['target'] ? $link['target'] : '_self';
            ?>

                <li class="<?php echo $link_slug; ?>">
                    <a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
                </li>

            <?php endif; ?>
        <?php endwhile; endif; ?>
    </ul>
</nav>