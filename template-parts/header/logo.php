<?php

    $logo = get_field('logo', 'options');
    $logo_small = get_field('logo_small', 'options');

?>

<div class="logo logo-mobile">
    <a href="<?php echo site_url('/'); ?>">
        <img src="<?php echo $logo_small['url']; ?>" alt="Heavy Restaurant Group" />
    </a>
</div>

<div class="logo logo-desktop" data-logo-large="<?php echo $logo['url']; ?>" data-logo-small="<?php echo $logo_small['url']; ?>">
    <a href="<?php echo site_url('/'); ?>">
        <img src="<?php echo $logo['url']; ?>" alt="Heavy Restaurant Group" />
    </a>
</div>