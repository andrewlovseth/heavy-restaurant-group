<?php

    $subcribe = get_field('subscribe', 'options');
    $headline = $subcribe['headline'];
    $form = $subcribe['form'];

?>

<div class="subscribe">
    <div class="headline">
        <h3><?php echo $headline; ?></h3>
    </div>
    
    <div class="subscribe-form">
        <?php echo $form; ?>
    </div>
</div>