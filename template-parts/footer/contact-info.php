<?php

    $contact_info = get_field('contact_info', 'options');
    $company_name = $contact_info['company_name'];
    $address = $contact_info['address'];
    $phone = $contact_info['phone'];
    $email = $contact_info['email'];
    $link = $contact_info['feedback_link'];

?>

<div class="contact-info copy copy-2">
    <div class="company-name">
        <p><?php echo $company_name; ?></p>
    </div>

    <div class="address">
        <p><?php echo $address; ?></p>
    </div>

    <div class="phone">
        <p><?php echo $phone; ?></p>
    </div>

    <div class="email">
        <p><a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></p>
    </div>

    <div class="feedback">
        <?php 
            if( $link ): 
            $link_url = $link['url'];
            $link_title = $link['title'];
            $link_target = $link['target'] ? $link['target'] : '_self';
        ?>

            <div class="cta">
                <a class="btn" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
            </div>

        <?php endif; ?>
    </div>
</div>