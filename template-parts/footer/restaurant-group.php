<div class="restaurant-group">

    <?php if(have_rows('restaurant_group', 'options')): while(have_rows('restaurant_group', 'options')): the_row(); ?>
        <?php
            $url = get_sub_field('url');
            $logo = get_sub_field('logo');
        ?>
    
        <div class="concept">
            <a href="<?php echo $url; ?>" target="window">
                <?php echo wp_get_attachment_image($logo['ID'], 'full'); ?>
            </a>
        </div>

    <?php endwhile; endif; ?>


</div>