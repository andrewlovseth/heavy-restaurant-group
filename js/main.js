import Header from './header.js';
import Carousel from './carousel.js';
import Animation from './animation.js';
import Content from './content.js';

(() => {
    Header.init();
    Carousel.init();
    Animation.init();
    Content.init();
})();
