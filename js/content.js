const Content = {
    tabs() {
        const tabs = document.querySelectorAll('.tabs');
        tabs.forEach((tabset) => {
            const tabButtons = tabset.querySelectorAll('.tab-list a');
            const tabPanels = Array.from(
                tabset.querySelectorAll('[role="tabpanel"]')
            );

            function handleTabClick(event) {
                event.preventDefault();

                tabPanels.forEach((panel) => {
                    panel.hidden = true;
                });

                tabButtons.forEach((tab) => {
                    tab.setAttribute('aria-selected', false);
                });

                event.currentTarget.setAttribute('aria-selected', true);

                const { id } = event.currentTarget;

                const tabPanel = tabPanels.find(
                    (panel) => panel.getAttribute('aria-labelledby') === id
                );
                tabPanel.hidden = false;
            }

            tabButtons.forEach((tabButton) => {
                tabButton.addEventListener('click', handleTabClick);
            });
        });
    },

    init: function () {
        this.tabs();
    },
};

export default Content;
