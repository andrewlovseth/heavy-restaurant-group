const Animation = {
    fadeIn() {
        const fadeInElems = document.querySelectorAll('.js-fade-in');

        let observer = new IntersectionObserver((entries) => {
            entries.forEach((entry) => {
                entry.target.classList.toggle('show', entry.isIntersecting);
                if (entry.isIntersecting) observer.unobserve(entry.target);
            });
        });

        fadeInElems.forEach((el) => {
            observer.observe(el);
        });
    },

    stickyHeader() {
        const snapPoint = document.querySelector('.header-snap-point');

        if (typeof snapPoint != 'undefined' && snapPoint != null) {
            const header = document.querySelector('.site-header');
            const logo = header.querySelector('.logo-desktop');
            const logoImage = logo.querySelector('img');
            const logoLarge = logo.dataset.logoLarge;
            const logoSmall = logo.dataset.logoSmall;

            const observer = new IntersectionObserver(function (entries, observer) {
                entries.forEach((entry) => {
                    header.classList.toggle('sticky', !entry.isIntersecting);
                    if (entry.boundingClientRect.y < 0) {
                        logo.classList.add('small');
                        logoImage.src = logoSmall;
                    } else {
                        logo.classList.remove('small');
                        logoImage.src = logoLarge;
                    }
                });
            });

            const options = {
                rootMargin: '0 0 0 0',
                threshold: 1,
            };

            observer.observe(snapPoint);
        }
    },

    conceptsFixedHeadlineDesktop() {
        const snapPoint = document.querySelector('.concepts-0-snap-point');

        if (typeof snapPoint != 'undefined' && snapPoint != null) {
            const sectionTitle = document.querySelector('.concepts-section-title-desktop');

            const observer = new IntersectionObserver(function (entries, observer) {
                entries.forEach((entry) => {
                    if (entry.boundingClientRect.y < 0) {
                        sectionTitle.classList.add('fixed');
                    } else {
                        sectionTitle.classList.remove('fixed');
                    }
                });
            });

            const options = {
                rootMargin: '0 0 0 0',
                threshold: 1,
            };

            observer.observe(snapPoint);
        }
    },

    conceptsModifyHeadlineDesktop() {
        const snapPoint = document.querySelector('.concepts-50-snap-point');

        if (typeof snapPoint != 'undefined' && snapPoint != null) {
            const sectionTitle = document.querySelector('.concepts-section-title-desktop');
            const initTitle = sectionTitle.dataset.initText;
            const modifiedTitle = sectionTitle.dataset.modifiedText;

            const observer = new IntersectionObserver(function (entries, observer) {
                entries.forEach((entry) => {
                    if (entry.boundingClientRect.y < 0) {
                        sectionTitle.textContent = modifiedTitle;
                    } else {
                        sectionTitle.textContent = initTitle;
                    }
                });
            });

            const options = {
                rootMargin: '0 0 0 0',
                threshold: 0.9,
            };

            observer.observe(snapPoint);
        }
    },

    conceptsParkHeadlineDesktop() {
        const snapPoint = document.querySelector('.concepts-100-snap-point');

        if (typeof snapPoint != 'undefined' && snapPoint != null) {
            const sectionHeader = document.querySelector('.concepts-section-header-desktop');
            const sectionTitle = document.querySelector('.concepts-section-title-desktop');

            const observer = new IntersectionObserver(function (entries, observer) {
                entries.forEach((entry) => {
                    sectionHeader.classList.toggle('park', entry.isIntersecting);
                    sectionTitle.classList.toggle('park', entry.isIntersecting);
                });
            });

            const options = {
                rootMargin: '0 0 1000px 0',
                threshold: 1,
            };

            observer.observe(snapPoint);
        }
    },

    conceptsModifyHeadlineMobile() {
        const snapPoint = document.querySelector('.concepts-mobile-snap-point');

        if (typeof snapPoint != 'undefined' && snapPoint != null) {
            const sectionTitle = document.querySelector('.concepts-section-title-mobile');
            const initTitle = sectionTitle.dataset.initText;
            const modifiedTitle = sectionTitle.dataset.modifiedText;

            const observer = new IntersectionObserver(function (entries, observer) {
                entries.forEach((entry) => {
                    if (entry.boundingClientRect.y < 0) {
                        sectionTitle.textContent = modifiedTitle;
                        console.log('Yes');
                    } else {
                        sectionTitle.textContent = initTitle;
                        console.log('No');
                    }
                });
            });

            const options = {
                rootMargin: '0 0 0 0',
                threshold: 0.1,
            };

            observer.observe(snapPoint);
        }
    },

    init: function () {
        this.fadeIn();
        this.stickyHeader();
        this.conceptsFixedHeadlineDesktop();
        this.conceptsModifyHeadlineDesktop();
        this.conceptsParkHeadlineDesktop();
        this.conceptsModifyHeadlineMobile();
    },
};

export default Animation;
