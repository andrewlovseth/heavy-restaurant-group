import './vendor/fslightbox.js';

const Carousel = {
    swiperSlider() {
        const swiper = new Swiper('.swiper', {
            speed: 5000,
            spaceBetween: 24,
            grabCursor: true,
            loop: true,
            centeredSlides: true,
            slidesPerView: 'auto',
            autoplay: {
                delay: 0,
                reverseDirection: false,
            },

            breakpoints: {
                768: {
                    spaceBetween: 48,
                },
            },
        });
    },

    swiperReverseSlider() {
        const swiper = new Swiper('.swiper-reverse', {
            speed: 5000,
            spaceBetween: 24,
            grabCursor: true,
            loop: true,
            centeredSlides: true,
            slidesPerView: 'auto',
            autoplay: {
                delay: 0,
                reverseDirection: true,
            },

            breakpoints: {
                768: {
                    spaceBetween: 48,
                },
            },
        });
    },

    peopleSlider() {
        const swiper = new Swiper('.js-people-slider', {
            speed: 5000,
            spaceBetween: 24,
            grabCursor: true,
            loop: true,
            centeredSlides: true,
            slidesPerView: 'auto',
            autoplay: {
                delay: 0,
                reverseDirection: false,
            },

            breakpoints: {
                768: {},
            },
        });
    },

    init: function () {
        this.swiperSlider();
        this.swiperReverseSlider();
        this.peopleSlider();
    },
};

export default Carousel;
