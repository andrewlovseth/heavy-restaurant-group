const Header = {
    hamburger() {
        const hamburger = document.querySelector('.js-nav-trigger');
        const nav = document.querySelector('.site-navigation');

        hamburger.addEventListener('click', (e) => {
            document.body.classList.toggle('nav-overlay-open');

            e.preventDefault();
        });
    },

    esc() {
        document.addEventListener('keyup', (e) => {
            if (e.key == 'Escape') {
                document.body.classList.remove('nav-overlay-open');
            }
        });
    },

    belowTheFold() {
        const header = document.querySelector('.site-header');

        const observer = new IntersectionObserver(function (entries, observer) {
            entries.forEach((entry) => {
                header.classList.toggle('scrolled', !entry.isIntersecting);
            });
        });

        observer.observe(header);
    },

    init: function () {
        this.hamburger();
        this.esc();
        this.belowTheFold();
    },
};

export default Header;
