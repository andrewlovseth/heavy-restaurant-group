<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<link  rel="stylesheet" href="https://unpkg.com/swiper@7/swiper-bundle.min.css" />
	<script src="https://unpkg.com/swiper@7/swiper-bundle.min.js"></script>

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>

<div id="page" class="site">

	<div class="header-snap-point snap-point"></div>

	<header class="site-header grid">
		<?php get_template_part('template-parts/header/logo'); ?>
		
		<?php get_template_part('template-parts/header/nav'); ?>

		<?php get_template_part('template-parts/header/social'); ?>
	</header>

	<main class="site-content">