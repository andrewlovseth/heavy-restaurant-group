<svg height="76" viewBox="0 0 76 76" width="76" xmlns="http://www.w3.org/2000/svg">
    <g fill="none" fill-rule="evenodd">
        <circle cx="38" cy="38" fill="#376c85" r="38"/><path d="m39.5 15.2333333v19.5326667l10 .0006667-12 24-12-24 10-.0006667v-19.5326667z" fill="#fff" fill-rule="nonzero"/>
    </g>
</svg>